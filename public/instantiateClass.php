<?php

require_once("../class/vehicle.php");
require_once("../class/train.php");
require_once("../class/plane.php");
require_once("../class/bike.php");
require_once("../class/car.php");


$train1 = new train("Electroputere Craiova");
$plane1 = new plane("Boeing 777");
$car1 = new car("Gri");
$bike1 = new bike("carbon");
echo "The model is {$train1->getModel()}" . "</br>";
echo "The model is {$plane1->getModel()}" . "</br>";
// $train->setModel("Blabla");
$train1 -> sound();
$plane1 -> sound();
$car1 -> sound();
$car1 -> color();
$bike1 -> sound();
$bike1 -> frame();

?>