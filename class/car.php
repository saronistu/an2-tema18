<?php 

    class car extends vehicle {
        private $color;
        
        public function __construct ($color) {
            vehicle::__construct("Skoda");
            $this -> color = $color;
        }

        public function setColor() {
            $this -> color = $color;
        }

        public function getColor() {
            return $this -> color;
        }

        public function color() {
            echo "The color of the car is " . $this -> color . "</br>";
        }

        public function sound() {
            echo " Sound of car." . "</br>";
        }
    }
?>