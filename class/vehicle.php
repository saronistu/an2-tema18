<?php 

    abstract class vehicle {
        private $model;
        
        public function __construct ($model) {
            $this -> model = $model;
        }

        public function setModel() {
            $this -> model = $model;
        }

        public function getModel() {
            return $this -> model;
        }

        abstract protected function sound();
    }
?>