<?php 

    class train {
        private $model;
        
        public function __construct ($model) {
            $this -> model = $model;
        }

        public function setModel() {
            $this -> model = $model;
        }

        public function getModel() {
            return $this -> model;
        }

        public function sound() {
            echo $this -> model . " makes tadam-tadam, tadam-tadam" . "</br>";
        }
    }
?>