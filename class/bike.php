<?php 

    class bike extends vehicle {
        private $frame;
        
        public function __construct ($frame) {
            vehicle::__construct("Devron");
            $this -> frame = $frame;
        }

        public function setFrame() {
            $this -> frame = $frame;
        }

        public function getFrame() {
            return $this -> frame;
        }

        public function frame() {
            echo "The bike's frame is made out of " . $this -> frame . "</br>";
        }

        public function sound() {
            echo " Suuuuuuuuuuuu". "</br>";
        }
    }
?>